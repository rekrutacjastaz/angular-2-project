import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {
    
    private _filter: string;
    private _filterBy: string;
    
    filterChanged: EventEmitter<string> = new EventEmitter<string>();
    filterByChanged: EventEmitter<string> = new EventEmitter<string>();
    
    setFilter(filter: string) {
        this._filter = filter;
        this.filterChanged.emit(this._filter);
    }
    
    setFilterBy(filterBy: string) {
        this._filterBy = filterBy;
        this.filterByChanged.emit(this._filterBy);
    }
    
    getFilter(): string {
        return this._filter;
    }
    
    getFilterBy(): string {
        return this._filterBy;
    }
}