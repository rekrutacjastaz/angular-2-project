import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'stars',
    templateUrl: 'app/shared/star_component/stars.component.html',
    styleUrls: ['app/shared/star_component/stars.component.css']
})
export class StarsComponent implements OnChanges {
    @Input() rating: number;
    starsWidth: number;
    @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();
    
    ngOnChanges(): void {
        let starsWidth = this.rating * 16;
        let spacesWidth = Math.floor(this.rating) * 4.25;
        this.starsWidth = starsWidth + spacesWidth; 
    }
    onClick() {
        this.ratingClicked.emit(`Ocena ${this.rating} została kliknięta!`);
    }
}