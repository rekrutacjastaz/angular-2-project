export interface IProduct {
    id: String;
    producer: string;
    model: string;
    code: string
    dimension: number;
    resolution: string;
    storage: string; 
    memory: string;
    processor: string;
    graphicsCard: string;
    os: string;
    amount: number;
    price: number; 
    description: string;
    rating: number;
    imageUrl: string;
}