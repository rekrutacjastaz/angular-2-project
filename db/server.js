var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var mongoose = require('mongoose');
mongoose.connect('mongodb://test:123456@ds023373.mlab.com:23373/products');

var Product = require('./models/product');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 5000;

var router = express.Router();

var initialProducts = require('../app/api/products/products.json') || undefined;

router.use(function (req, res, next) {
    console.log('Request sent');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE')
    next();
});

router.get('/', function (req, res) {
    res.json({ message: 'API server' });
});

router.route('/products')
    .post(function (req, res) {
        var product = new Product();
        product.producer = req.body.producer;
        product.model = req.body.model;
        product.code = req.body.code;
        product.dimension = req.body.dimension;
        product.resolution = req.body.resolution;
        product.storage = req.body.storage;
        product.memory = req.body.memory;
        product.processor = req.body.processor;
        product.graphicsCard = req.body.graphicsCard;
        product.os = req.body.os;
        product.amount = req.body.amount;
        product.price = req.body.price;
        product.description = req.body.description;
        product.rating = req.body.rating;
        product.imageUrl = req.body.imageUrl;

        product.save(function (err) {
            if (err) {
                res.send(err);
            }
            res.json({ message: 'Product created' });
        });
    })
    .get(function (req, res) {
        Product.find(function (err, products) {
            if (err) {
                res.send(err);
            }
            res.json(products);
        });
    });

router.route('/products/:product_id')
    .get(function (req, res) {
        Product.findById(req.params.product_id, function (err, product) {
            if (err) {
                res.send(err);
            }
            res.json(product);
        });
    })
    .put(function (req, res) {
        Product.findById(req.params.product_id, function (err, product) {

            if (err) {
                res.send(err);
            }
            product.producer = req.body.producer || product.producer;
            product.model = req.body.model || product.model;
            product.code = req.body.code || product.code;
            product.dimension = req.body.dimension || product.dimension;
            product.resolution = req.body.resolution || product.resolution;
            product.storage = req.body.storage || product.storage;
            product.memory = req.body.memory || product.memory;
            product.processor = req.body.processor || product.processor;
            product.graphicsCard = req.body.graphicsCard || product.graphicsCard;
            product.os = req.body.os || product.os;
            product.amount = req.body.amount || product.prodamountucer;
            product.price = req.body.price || product.price;
            product.description = req.body.description || product.description;
            product.rating = req.body.rating || product.rating;
            product.imageUrl = req.body.imageUrl || product.imageUrl;

            product.save(function (err) {
                if (err) {
                    res.send(err);
                }
                res.json({ message: 'Product updated' });
            });
        });
    })
    .delete(function (req, res) {
        Product.remove({
            _id: req.params.product_id
        }, function (err, product) {
            if (err) {
                res.send(err);
            }
            res.json({ message: 'Product deleted' });
        });
    });

router.route('/initdb')
    .get(function (req, res) {
        for (let p of initialProducts) {
            var product = new Product();
            product.producer = p.producer;
            product.model = p.model;
            product.code = p.code;
            product.dimension = p.dimension;
            product.resolution = p.resolution;
            product.storage = p.storage;
            product.memory = p.memory;
            product.processor = p.processor;
            product.graphicsCard = p.graphicsCard;
            product.os = p.os;
            product.amount = p.amount;
            product.price = p.price;
            product.description = p.description;
            product.rating = p.rating;
            product.imageUrl = p.imageUrl;

            product.save(function (err) {
                if (err) {
                    res.send(err);
                }
            });
        }
        res.json({ message: 'Database initialized' });
    });

router.route('/cleardb')
    .get(function (req, res) {
        Product.find(function (err, products) {
            if (err) {
                res.send(err);
            }
            for (let product of products) {
                Product.remove({
                    _id: product._id
                }, function (err, product) {
                    if (err) {
                        res.send(err);
                    }
                });
            }
            res.json({ message: 'All products deleted' });
        });
    });


app.use('/api', router);

app.listen(port);
console.log('Product API server listening on port: ' + port);