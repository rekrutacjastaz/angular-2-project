# Angular 2 project #

Demo: wkrótce

## Features

* baza danych mongoDB na zewnętrznym serwerze (uruchamiana przy starcie aplikacji)
* RESTful API zbudowane z wykorzysaniem biblioteki express
* serwis odpytujący API o dane i komponenty subskrybujące się na strumienie danych
* dodawanie i usuwanie produktów z bazy
* podstawowa walidacja w formularzu dodawania produktu
    * przycisk Dodaj działa tylko przy braku błędów w formularzu
    * chmurki informacyjne dla wymaganych pól
    * ostylowanie niepoprawnych pól
    * kod produktu częściowo wyliczany na bazie aktualnego stanu formularza
* wykorzystanie bootstrapa
* animacja przy ładowaniu aplikacji i w widoku /home
* wykorzystanie dyrektyw *ngIf i *ngFor
* binding: interpolacja (property binding?), 2-way data binding, event binding
* wyświetlanie i ukrywnaie obrazka
    * zmiana treści przycisku
    * wyśrodkowanie treści w pionie po pokazaniu
    * najechanie na obrazek wyświetla producenta i model
* custom pipe: filtrowanie, sortowanie, przeliczanie walut
* wykorzystanie interfejsów (IProduct), enumeratorów (Order, Method)
* filtrowanie listy produktów po różnych kryteriach
    * w czasie rzeczywistym
    * wielkość liter nie ma znaczenia
    * sortowanie działa na odfiltrowanych danych
    * po powrocie z widoku szczegółów lista nadal jest odfiltrowana, a parametry
	wyszukiwania są w polach filtrowana
    * wymiana danych między komponentami (formularz w app, filtrowanie w product-list)
* nested component: star-component: ocena wyświetlana w formie gwiazdek
    * po najechaniu myszą wyświetla się dokładna wartość liczbowa
    * wykorzystanie dekoratorów @Input i @Output do wymiany danych z container component
* serwisy: dostęp do danych o produktach, wymiana danych między komponentami, przeliczanie 	walut
    * dependency injection
    * serwis jako singleton
    * serwisu działają z wykorzystaniem strumieni, które subskrybują się odpow. komponenty
    * lista produktów pozyskiwana poprzez zapytanie HTTP GET
        * wykorzystanie programowania reaktywnego [map(response -> json]
        * asynchroniczne zapytania http do bazy produktów (serwisu) z wykorzystaniem 		
    * dataService - EventEmitter i komponenty subskrybujące ten eventlifecycle hooks inicjalizacja przedmiotów na liście (nie robimy tego w konstr.)
        * subskrypcja obserwabli (dane, logowanie odpowiedzi, błąd 
* aktualny kurs dolara pobierany z zewnętrznego serwisu poprzez zapytanie HTTP
    * czas odpowiedzi 3.55 ms
    * o kurs odpytujemy z wykorzystaniem lifecycle hooks w komponencie product-list
	i przekazujemy go jako parametr do pipe z przeliczaniem walut (tylko jedno 		zapytanie na całą listę)
    * cena z bazy danych przeliczana na złotówki wg obecnego kursu dolara
* sortowanie kolumn tabeli (rosnąco i malejąco)
    * różne sposoby sortowania: numeryczny i leksykalny
    * sortowanie jako pipe
    * parametry sortowania przekazywane jako parametry do pipe
    * glyf obok nazwy kolumny pokazujący aktualne sortowanie
    * łapka jako wskaźnik sugeruje możliwość kliknięcia
* routing i widoki:
    * menu (aktywna zakładka jest podświetlana)
    * formularz filtrowania tylko dla widoku z listą produktów
    * "routning z kodu" - powrót do listy prod. z widoku szczegółów (przycisk powrót)
    * parametr routingu (:id dla klikniętego produktu na liście - wskaźnik sugeruje 	możliwość kliknięcia)
* widok ze szczegółami produktu
    * ponowne wykorzystanie nested component starts (shared)
    * subskrybcja na pojedyńczy produkt
* enkapsulacja arkuszy stylów komponentów
* html komponentów w osobnych plikach
* wbudowane pipes (uppercase)
* ...?

